/*
 *
 *  * Copyright (C) 2023 FRANCOIS DEVILEZ
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 *
 */

create TYPE log_level as ENUM ('INFO', 'ERROR');
create TYPE request_method as ENUM ('GET', 'POST', 'NONE');
create CAST (varchar AS log_level) WITH INOUT AS IMPLICIT;
create CAST (varchar AS request_method) WITH INOUT AS IMPLICIT;

create table sun.logs
(
    id             SERIAL PRIMARY KEY,
    message        VARCHAR(2000)  NOT NULL,
    endpoint       VARCHAR(200)   NOT NULL,
    request_method request_method NOT NULL,
    log_level      log_level      NOT NULL,
    created_at     TIMESTAMP DEFAULT now(),
    updated_at     TIMESTAMP DEFAULT now(),
    project_id     INT            NOT NUll,
    CONSTRAINT projectId_fk
        FOREIGN KEY (project_id)
            REFERENCES sun.projects (id)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
);

CREATE TRIGGER update_project_updated_at
    BEFORE UPDATE
    ON sun.logs
    FOR EACH ROW
EXECUTE PROCEDURE update_modified_column();