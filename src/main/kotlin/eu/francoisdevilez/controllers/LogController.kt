/*
 *
 *  * Copyright (C) 2023 FRANCOIS DEVILEZ
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 *
 */

package eu.francoisdevilez.controllers

import eu.francoisdevilez.managers.LogManager
import eu.francoisdevilez.models.dto.LogDTO
import io.micronaut.data.model.Pageable
import io.micronaut.data.model.Sort
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*
import jakarta.validation.Valid
import java.time.OffsetDateTime

@Controller(value = "/log")
class LogController(
    private val logManager: LogManager
) {
    @Get(produces = [MediaType.APPLICATION_JSON])
    suspend fun getLogs(pageable: Pageable): HttpResponse<Map<String, Any>> = HttpResponse.ok(
        mapOf(
            "totalNumberOfLogs" to logManager.countLogs(),
            "logs" to logManager.getLogs(
                Pageable.from(pageable.number, pageable.size, Sort.of(Sort.Order.desc("id")))
            ).content
        )
    )

    @Post(produces = [MediaType.APPLICATION_JSON], consumes = [MediaType.APPLICATION_JSON])
    suspend fun saveNewLog(@Body @Valid logDTO: LogDTO): HttpResponse<LogDTO> =
        HttpResponse.created(this.logManager.saveLog(logDTO))

    @Get(value = "/statistics", produces = [MediaType.APPLICATION_JSON])
    suspend fun getStatistics(
        @QueryValue start: OffsetDateTime,
        @QueryValue end: OffsetDateTime
    ): HttpResponse<Map<String, Any>> =
        HttpResponse.ok(this.logManager.getStatistics(start, end))
}