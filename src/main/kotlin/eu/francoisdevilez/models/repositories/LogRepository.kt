/*
 *
 *  * Copyright (C) 2023 FRANCOIS DEVILEZ
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  
 *
 */

package eu.francoisdevilez.models.repositories

import eu.francoisdevilez.models.dto.AverageNumberOfLogsPerProjectDTO
import eu.francoisdevilez.models.dto.LogsPerDayDTO
import eu.francoisdevilez.models.dto.LogsPerProjectDTO
import eu.francoisdevilez.models.entities.Log
import io.micronaut.data.annotation.Join
import io.micronaut.data.annotation.Query
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.r2dbc.annotation.R2dbcRepository
import io.micronaut.data.repository.kotlin.CoroutinePageableCrudRepository
import java.time.OffsetDateTime

@R2dbcRepository(dialect = Dialect.POSTGRES)
interface LogRepository : CoroutinePageableCrudRepository<Log, Long> {
    @Join(value = "project", type = Join.Type.LEFT_FETCH)
    override suspend fun findAll(pageable: Pageable): Page<Log>

    @Query(
        value = "SELECT p.name AS project_name, COUNT(l) AS total FROM sun.logs l " +
                "JOIN sun.projects p ON l.project_id = p.id " +
                "WHERE l.created_at BETWEEN SYMMETRIC :start AND :end " +
                "GROUP BY p.id, p.name;"
    )
    suspend fun countLogsPerProject(start: OffsetDateTime, end: OffsetDateTime): List<LogsPerProjectDTO>

    @Query(
        value = "SELECT DATE(l.created_at) AS date, COUNT(l) AS total FROM sun.logs l " +
                "WHERE l.created_at BETWEEN SYMMETRIC :start AND :end " +
                "GROUP BY DATE(l.created_at)" +
                "ORDER BY date"
    )
    suspend fun countLogsPerDay(start: OffsetDateTime, end: OffsetDateTime): List<LogsPerDayDTO>

    @Query(
        value = "SELECT p.name AS project_name, ROUND(COUNT(*) / COUNT(DISTINCT(DATE(l.created_at)))) AS average " +
                "FROM sun.projects p " +
                "LEFT JOIN sun.logs l ON l.project_id = p.id " +
                "WHERE l.created_at BETWEEN SYMMETRIC :start AND :end " +
                "GROUP BY p.id, p.name "
    )
    suspend fun averageNumberOfLogsPerProject(
        start: OffsetDateTime,
        end: OffsetDateTime
    ): List<AverageNumberOfLogsPerProjectDTO>
}