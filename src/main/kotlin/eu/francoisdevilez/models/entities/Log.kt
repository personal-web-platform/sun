/*
 *
 *  * Copyright (C) 2023 FRANCOIS DEVILEZ
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 *
 */

package eu.francoisdevilez.models.entities

import eu.francoisdevilez.models.dto.LogDTO
import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import io.micronaut.data.annotation.Relation
import java.time.OffsetDateTime

enum class LogLevels {
    INFO, ERROR;
}

enum class RequestMethod {
    GET, POST, NONE
}

@MappedEntity(schema = "sun", value = "logs")
data class Log(
        val message: String,
        val endpoint: String,
        val requestMethod: RequestMethod,
        val createdAt: OffsetDateTime?,
        val updatedAt: OffsetDateTime?,
        val logLevel: LogLevels,
        @Relation(Relation.Kind.MANY_TO_ONE)
        val project: Project
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    fun toDTO() = LogDTO(
            id = id,
            message = message,
            endpoint = endpoint,
            requestMethod = requestMethod.name,
            logLevel = logLevel.name,
            createdAt = createdAt,
            updatedAt = updatedAt,
            project = project.name
    )
}