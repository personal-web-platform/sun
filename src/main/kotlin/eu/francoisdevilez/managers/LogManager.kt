/*
 *
 *  * Copyright (C) 2023 FRANCOIS DEVILEZ
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 *
 */

package eu.francoisdevilez.managers

import eu.francoisdevilez.models.dto.LogDTO
import eu.francoisdevilez.models.entities.Log
import eu.francoisdevilez.models.entities.LogLevels
import eu.francoisdevilez.models.entities.RequestMethod
import eu.francoisdevilez.models.repositories.LogRepository
import eu.francoisdevilez.models.repositories.ProjectRepository
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import jakarta.inject.Singleton
import java.time.OffsetDateTime

@Singleton
class LogManager(
    private val logRepository: LogRepository, private val projectRepository: ProjectRepository
) {
    suspend fun getLogs(pageable: Pageable): Page<LogDTO> = logRepository.findAll(pageable).map { it.toDTO() }

    suspend fun countLogs(): Long = logRepository.count()

    suspend fun saveLog(logDTO: LogDTO): LogDTO = this.logRepository.save(
        Log(
            logDTO.message,
            logDTO.endpoint,
            RequestMethod.valueOf(logDTO.requestMethod),
            OffsetDateTime.now(),
            OffsetDateTime.now(),
            LogLevels.valueOf(logDTO.logLevel),
            projectRepository.findByName(logDTO.project.lowercase())
                ?: throw IllegalStateException("Could not find project ${logDTO.project.lowercase()}")
        )
    ).toDTO()

    suspend fun getStatistics(start: OffsetDateTime, end: OffsetDateTime): Map<String, Any> = mapOf(
        "numberOfLogs" to this.logRepository.count(),
        "numberOfProjects" to this.projectRepository.count(),
        "numberOfLogsPerProject" to this.logRepository.countLogsPerProject(start, end),
        "numberOfLogsPerDay" to this.logRepository.countLogsPerDay(start, end),
        "averageNumberOfLogsPerProject" to this.logRepository.averageNumberOfLogsPerProject(start, end)
    )
}